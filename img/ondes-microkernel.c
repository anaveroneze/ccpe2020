|\color{red}{static inline double CPML4}|(double vp, double dump, double alpha, 
	double kappa, double phidum, double dx, double dt, double x1, 
	double x2, double x3, double x4) {
   double a, b;
   b = exp(-(vp*dump/kappa+alpha)*dt);
   a = 0.0;
   if(abs(vp*dump) > 0.000001) 
      a = vp*dump*(b-1.0)/(kappa*(vp*dump + kappa*alpha));
   return b*phidum + a*((9./8.)*(x2-x1)/dx - (1./24.)*(x4 - x3)/dx);
}
